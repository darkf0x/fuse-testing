# FUSE Testing

## TO RUN:

1) Start your FUSE server `./cs1550 -d testmount`.

2) In another window, while your FUSE server is running, with 'test.sh' in the
   'example' directory, run `./test.sh testmount`.

3) By default, 'test.sh' will terminate when a test fails to avoid corrupting
   your mount directory. If you'd like, you can ignore failed tests and run them
   all no matter what. Just run with `./test.sh testmount -i` to ignore fails.
   
4) Feel free to add more tests on your own branch and submit merge requests for
   them, I will be pushing new tests as I think of them. Please don't commit
   directly to master branch.